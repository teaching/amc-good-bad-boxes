# README

Here you find all the source files to generate this image (in PDF format) to be included in Multiple Choice exams:

![good-bad-boxes](good-bad-boxes.png)

The source images in SVG format have been all generated with Adobe Illustrator but can be modified with any other compatible software. The size of the images is 9x9 mm and the size of the checkbox in it is 5x5 mm.

These source images have to be converted to pdf-images. This can be done with `rsvg-convert` (on Mac OS install  `port install librsvg` or `brew install librsvg`; on Fedora Linux `dnf install librsvg2-tools`):

```bash
for svg in src/*.svg; do
	pdf="${svg//svg/pdf}"
	rsvg-convert -f pdf -o "$pdf" "$svg"
done
```

Finally the "good-bad-boxes" pdf-image can be generated as follow:

```bash
pdflatex good-bad-boxes.tex
```

The result can be seen at the top of this document. Of course the image on the top is a PNG, which has been created as follow:

```bash
convert -density 300 good-bad-boxes.pdf good-bad-boxes.png
```

Include the pdf-image in you Multiple Choice exam as follow:

```tex
%\usepackage{graphicx} % should be already included
\includegraphics[width=\textwidth]{good-bad-boxes}
```